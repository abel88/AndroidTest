package com.example.ventas.appcuestionario;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText usuario,password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        usuario= (EditText) findViewById(R.id.usuario);
        password= (EditText) findViewById(R.id.password);
    }

    public void login(View view){

        String user=usuario.getText().toString();
        String pass=password.getText().toString();
        if (user.endsWith("Administrador")&&pass.endsWith("123")){
            Intent i=new Intent(this, CrearCuestionarioActivity.class);
            startActivity(i);

        }else {
            Toast toast =Toast.makeText(this,"ingrese de nuevo el usuario",Toast.LENGTH_SHORT);
            toast.show();
        }
        if (user.endsWith("Alumno")&&pass.endsWith("123")){
            Intent i=new Intent(this,CuestionarioActivity.class);
            startActivity(i);
        }else {
            Toast toast=Toast.makeText(this,"ingrese datos",Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}
