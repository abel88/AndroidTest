package com.example.ventas.appcuestionario;

import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class CuestionarioActivity extends AppCompatActivity {

    private  int codigopregunta1[]={
      R.id.rd1,R.id.rd2,R.id.rd3
    };
    private int preguntacorrecta;
    private int preguntaactual;
    private String[] todaslaspreguntas;
    private TextView txtpregunta1;
    private RadioGroup grupo;
    private boolean[]correctos;
    private  Button btn1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuestionario);

        txtpregunta1= (TextView) findViewById(R.id.txtpregunta1);
        grupo= (RadioGroup) findViewById(R.id.rg1);

         btn1= (Button) findViewById(R.id.btn1);
        todaslaspreguntas=getResources().getStringArray(R.array.todaslaspreguntas);
        correctos=new boolean[todaslaspreguntas.length];
        preguntaactual=0;
        Mostrarunapregunta();


        //se crea una variable correcto instanciando lo que es el recuros en el archivo string
       // final int correcto =getResources().getInteger(R.integer.correcto);
        //se creo la variable del gruoo
        //TODO: cuando se le da {@link  que para a la siguiente pregutna}

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //este funncion apunta al que radio buton esta seleccionado
                int id=grupo.getCheckedRadioButtonId();
                int respuesta=-1;
                for (int i=0;i<codigopregunta1.length;i++){
                    if (codigopregunta1[i]==id){
                       respuesta=i;

                    }
                }
                /*
                if (respuesta==preguntacorrecta){
                    Toast.makeText(CuestionarioActivity.this, "Respuesta Correcto", Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(CuestionarioActivity.this, "Incorrecto", Toast.LENGTH_SHORT).show();
                }*/
                correctos[preguntacorrecta]=(respuesta==preguntacorrecta);


                if (preguntaactual<todaslaspreguntas.length-1){
                    preguntaactual++;
                    Mostrarunapregunta();
                }
                else {
                    int correcto=0;
                    int incorrecto=0;
                    for (boolean b:correctos){
                        if (b)correcto++;
                        else incorrecto++;
                    }
                    String resultado=String.format("correctos: %d -- Incorrectos: %d ",correcto,incorrecto);
                    Toast.makeText(CuestionarioActivity.this, resultado, Toast.LENGTH_SHORT).show();
                    finish();
                }

                /*for (int i=0;i<correctos.length;i++){
                    Log.i("correcto",String.format("Respuesta correctos %d: %b",i,correctos[i]));
                }*/

            }
        });
        //TODO: se puede regresar atras

    }

    private void Mostrarunapregunta() {
        String q= todaslaspreguntas[preguntaactual];
        String[]partes=q.split(";");
        grupo.clearCheck();

        txtpregunta1.setText(partes[0]);

        //String[] respuestas=getResources().getStringArray(R.array.respuesas1);

        for (int i=0;i<codigopregunta1.length;i++){
            RadioButton rb= (RadioButton) findViewById(codigopregunta1[i]);
            String respuesta=partes[i+1];
            if (respuesta.charAt(0)=='*'){
                preguntacorrecta=i;
                respuesta=respuesta.substring(1+1);

            }
            rb.setText(respuesta);
        }
        if (preguntaactual==todaslaspreguntas.length-1){
            btn1.setText(R.string.terminar);
        }
    }
}
